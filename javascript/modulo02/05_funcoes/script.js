function areaQuadrado(lado){
  return lado * lado;
}

console.log(areaQuadrado(4));

function pi(){
  return 3.14;
}

var total = 5 * pi(); 

// TODO: Qual vai ser a saída disso e porquê?
console.log(console.log(console.log('oi'))); // mostra o retorno da função
console.log(pi); // mostra o conteúdo da função

// Função para calcular IMC
function imc(peso, altura){
  var imc = peso / (altura * altura);
  return imc;
}

imc(50, 1.40);

console.log(imc(60, 1.8));

// Função com condicional
function corFavorita(cor){
  if(cor === 'azul'){
    return 'Eu gosto do céu.'
  } else if(cor === 'verde'){
    return 'Eu gosto de mato.'
  } else {
    return 'Eu não gosto de cores.'
  }
}

/* Funções como parâmetros.
EXEMPLO: addEventListener('click', function(){...});
*/

function imc2(peso, altura){
  const imc = peso / (altura ** 2);
  return console.log(imc);
}

imc2(50, 1.35);